﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillManager : MonoBehaviour
{
    public static bool skillActivate;
    public static bool activateShield;
    public static bool activateCritStrike;
    public static bool activateHeal;

    public static bool skillIsCooldown;
    public static int cooldownSkill;

    private void Awake()
    {
        cooldownSkill = 0;
    }

    private void Start()
    {
        skillActivate = false;
        activateShield = false;
        activateCritStrike = false;
        activateHeal = false;
    }

    private void Update()
    {
        // SKILL COOLDOWN //

        if (skillIsCooldown && cooldownSkill == 0)
        {
            skillIsCooldown = false;
            // Tunjukkan salah satu random skill
            UIManager.HideCooldown();
            UIManager.ShuffleSkill();
        }

        // SKILL COOLDOWN //
    }

    public static void SkillShield()
    {
        // Null any damage this turn
        if (GameManager.attack)
        {
            RPSManager.ctrDmg = 0;
        }

        if (GameManager.defend)
        {
            RPSManager.attackDmg = 0;
        }
    }

    public static void SkillCritStrike()
    {
        // Double any damage you make this turn

        RPSManager.ctrDmg *= 2;
        RPSManager.attackDmg *= 2;
    }

    public static void SkillHeal()
    {
        // Heal 2 this turn
        GameManager.playerHealth += 2;

        UIManager.playerHealthText.text = GameManager.playerHealth.ToString();
    }

    //---------- ABOVE IS SKILL ----------//


    public static void SkillActivated()                         // Digunakan di RPSManager di Result()
    {
        if (activateShield)
        {
            SkillShield();
            cooldownSkill = 3;
            FindObjectOfType<SoundManager>().Play("UsingSkill");
            UIManager.DisableInfoSkillPanel(false);
            UIManager.infoSkillText.text = "You Activate Shield";
            print("Shield Active");
        }

        else if (activateCritStrike)
        {
            SkillCritStrike();
            cooldownSkill = 3;
            FindObjectOfType<SoundManager>().Play("UsingSkill");
            UIManager.DisableInfoSkillPanel(false);
            UIManager.infoSkillText.text = "You Activate Critical Strike";
            print("CritStrike Active");
        }

        else if (activateHeal)
        {
            SkillHeal();
            cooldownSkill = 4;
            FindObjectOfType<SoundManager>().Play("UsingSkill");
            UIManager.DisableInfoSkillPanel(false);
            UIManager.infoSkillText.text = "You Activate Heal";
            print("Heal Active");
        }

        skillIsCooldown = true;

        UIManager.DisableAllSkillBtn();                             // Disable semua skill btn dan shuffle Btn. Diambil dari UIManager
    }

    public static void ResetSkill()                                 // Digunakan pada RPSManager di Action() dan UIManager di ShuffleSkillBtn
    {
        skillActivate = false;
        activateShield = false;
        activateCritStrike = false;
        activateHeal = false;

        UIManager.activeText.SetActive(false);
        print("Semua Skill Direset");
    }
}
