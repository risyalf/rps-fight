﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour
{
    public static Animator playerAnimator;
    public static Animator enemyAnimator;

    public static Animator playerHealthAnimator;
    public static Animator enemyHealthAnimator;

    private void Start()
    {
        playerAnimator = GameObject.Find("Player").GetComponent<Animator>();
        enemyAnimator = GameObject.Find("Enemy").GetComponent<Animator>();

        playerHealthAnimator = GameObject.Find("PlayerHealth").GetComponent<Animator>();
        enemyHealthAnimator = GameObject.Find("EnemyHealth").GetComponent<Animator>();
    }

    public static void Animation(Animator player, string playerCondition, Animator enemy, string enemyCondition)
    {
        player.SetTrigger(playerCondition);
        enemy.SetTrigger(enemyCondition);
    }

    public static void HealthTakingDamageAnimation(Animator who)
    {
        who.SetTrigger("isTakingDamage");
    }
}
