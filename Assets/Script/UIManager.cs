﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public static Text commentText;
    public static Text timerCount;
    public static Text timerText;

    public static Text playerHealthText;
    public static Text enemyHealthText;

    public static GameObject shieldSkillBtn;
    public static GameObject critStrikeSkillBtn;
    public static GameObject healSkillBtn;
    public static GameObject shuffleSkillBtn;
    public static GameObject activeText;
    public static GameObject cdText;

    public static GameObject infoSkill;
    public static Text infoSkillText;

    public static GameObject cooldownSkillPanel;
    public static Text cooldownSkilTxt;

    public static bool shuffleIsPressed;                                        // Digunakan di GameManager pada NewState()
    public bool cobaShuffleIsPressed;

    // UI RPS di atas player dan musuh, Timer, UI RPS, dan UI Skill

    public static GameObject[] allButtons;

    public static GameObject RestartPanel;

    private void Start()
    {
        allButtons = GameObject.FindGameObjectsWithTag("UIButton");
        commentText = GameObject.Find("CommentText").GetComponent<Text>();
        timerCount = GameObject.Find("TimerCount").GetComponent<Text>();
        timerText = GameObject.Find("TimerText").GetComponent<Text>();

        enemyHealthText = GameObject.Find("EnemyHealthText").GetComponent<Text>();
        playerHealthText = GameObject.Find("PlayerHealthText").GetComponent<Text>();
        RestartPanel = GameObject.Find("RestartPanel");

        shieldSkillBtn = GameObject.Find("ShieldSkillBtn");
        critStrikeSkillBtn = GameObject.Find("CritStrikeSkillBtn");
        healSkillBtn = GameObject.Find("HealSkillBtn");
        shuffleSkillBtn = GameObject.Find("ShuffleSkillBtn");
        activeText = GameObject.Find("ActiveText");
        cdText = GameObject.Find("TextCd");

        infoSkill = GameObject.Find("InformationSkill");
        infoSkillText = GameObject.Find("InfoSkillText").GetComponent<Text>();

        cooldownSkillPanel = GameObject.Find("CooldownSkillPanel");
        cooldownSkilTxt = GameObject.Find("CooldownSkilTxt").GetComponent<Text>();

        RestartPanelControl(false);
        activeText.SetActive(false);

        EnableAllButton();
        HideCooldown();
        ShuffleSkill();
        shuffleIsPressed = false;
        DisableShuffleBtn(false);
        DisableInfoSkillPanel(true);
    }

    private void Update()
    {
        cobaShuffleIsPressed = shuffleIsPressed;                                    // Hanya untuk percobaan

        if (shuffleIsPressed)
        {
            shuffleSkillBtn.GetComponentInChildren<Text>().enabled = false;
        }

        if (!shuffleIsPressed)
        {
            shuffleSkillBtn.GetComponentInChildren<Text>().enabled = true;
        }
    }

    public static void EnableAllButton()
    {
        foreach (GameObject btn in allButtons)
        {
            Button theBtn = btn.GetComponent<Button>();
            theBtn.enabled = true;
        }
    }

    public static void DisableAllButton()
    {
        foreach (GameObject btn in allButtons)
        {
            Button theBtn = btn.GetComponent<Button>();
            theBtn.enabled = false;
        }
    }

    public void CommentText(string rpsComment, bool isReset)                                     
    {
        if (isReset)
        {
            commentText.text = "";
        }

        StartCoroutine(PlayText(rpsComment, commentText));
    }

    public void TimerText(string comment)
    {
        timerText.text = "";

        StartCoroutine(PlayText(comment, timerText));

        if (GameManager.defend)
        {
            AnimationManager.Animation(AnimationManager.playerAnimator, "isTakingDamage", AnimationManager.enemyAnimator, "isAttacking");
        }

        StartCoroutine(DeleteTimerText());
    }

    IEnumerator PlayText(string str, Text whichText)
    {
        foreach (char c in str)
        {            
            whichText.text += c;
            yield return new WaitForSeconds(0.025f);
        }
    }

    IEnumerator DeleteTimerText()
    {
        yield return new WaitForSeconds(3f);

        timerText.text = "";
    }

    public static void ShowHealth()
    {
        if (GameManager.playerHealth < 0)
        {
            GameManager.playerHealth = 0;
        }

        if (GameManager.enemyHealth < 0)
        {
            GameManager.enemyHealth = 0;
        }

        playerHealthText.text = GameManager.playerHealth.ToString();
        enemyHealthText.text = GameManager.enemyHealth.ToString();
    }

    public static void ShowTimer()
    {
        timerCount.text = Mathf.RoundToInt(GameManager.timer).ToString();
    }

    public void YesRestart()
    {
        SceneManager.LoadScene("MainScene");
    }

    public void NoReturnPls()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public static void RestartPanelControl(bool enable)
    {
        if (enable)
        {
            RestartPanel.SetActive(true);
        }

        else if (!enable)
        {
            RestartPanel.SetActive(false);
        }
    }


    //--------- UI SKILL BUTTON CONTROL ---------//

    public static void DisableAllSkillBtn()                                          // Disable semua btn skill dan shuffle btn
    {
        // Menunjukkan berapa lama cooldownnya

        if (SkillManager.cooldownSkill > 0)
        {
            shieldSkillBtn.SetActive(false);
            critStrikeSkillBtn.SetActive(false);
            healSkillBtn.SetActive(false);

            ShowCooldown();
            //DisableInfoSkillPanel(false);
        }
    }

    public static void ShowCooldown()                                           // Digunakan pada SkillManager pada SkillActivated
    {
        cooldownSkillPanel.SetActive(true);

        cooldownSkilTxt.text = "";
        cooldownSkilTxt.text = SkillManager.cooldownSkill.ToString();           // cooldown harus ada yg mengurangi. Akan dikurangi di GameManager pada NewState()
    }

    public static void HideCooldown()                                            // Digunakan pada ShuffleSkill
    {
        cooldownSkillPanel.SetActive(false);
    }

    public void ShuffleSkillBtn()                                               // Digunakan pada Button Shuffle
    {
        if (SkillManager.cooldownSkill <= 0)
        {
            ShuffleSkill();
            shuffleIsPressed = true;
        }
    }

    public static void DisableShuffleBtn(bool yes)
    {
        if (yes)
        {
            shuffleSkillBtn.GetComponent<Button>().enabled = false;

            cdText.SetActive(true);
        }

        else if (!yes)
        {
            shuffleSkillBtn.GetComponent<Button>().enabled = true;
                        
            cdText.SetActive(false);
        }
    }

    public static void ShuffleSkill()
    {
        int rndmNbr = Random.Range(0, 3);

        if (!shuffleIsPressed) 
        {
            if (rndmNbr == 0)
            {
                shieldSkillBtn.SetActive(true);
                critStrikeSkillBtn.SetActive(false);
                healSkillBtn.SetActive(false);

                print("Shuffle Skill");
            }

            else if (rndmNbr == 1)
            {
                shieldSkillBtn.SetActive(false);
                critStrikeSkillBtn.SetActive(true);
                healSkillBtn.SetActive(false);

                print("Shuffle Skill");
            }

            else if (rndmNbr == 2)
            {
                shieldSkillBtn.SetActive(false);
                critStrikeSkillBtn.SetActive(false);
                healSkillBtn.SetActive(true);

                print("Shuffle Skill");
            }

            SkillManager.ResetSkill();
        }
    }

    public static void ShuffleBtnIsPressed(bool yes)                                // Digunakan di GameManager pada NewState()
    {
        if (yes) 
        { 
            shuffleIsPressed = true;                                           // bila true maka menjadi false
        }

        else if (!yes)
        {
            shuffleIsPressed = false;                                            // bila false maka menjadi true
        }
    }

    public static void DisableInfoSkillPanel(bool choose)
    {
        if (choose)
        {
            infoSkill.SetActive(false);
        }

        else
        {
            infoSkill.SetActive(true);
        }
    }

    //--------- UI SKILL BUTTON CONTROL ---------//
}