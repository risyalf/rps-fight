﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static bool attack;
    public static bool defend;
    public static bool timesUp;

    public static int playerHealth;
    public static int enemyHealth;
    public static float timer;

    UIManager uiManager;

    static bool isEnding;

    float restartTimer;

    private void Start()
    {
        uiManager = GameObject.Find("UIManager").GetComponent<UIManager>();
        
        NewState();

        playerHealth = 10;
        enemyHealth = 10;

        UIManager.ShowHealth();

        restartTimer = 4.5f;
        timer = 15f;

        isEnding = false;
        timesUp = false;
    }

    private void LateUpdate()
    {
        if (!isEnding)
        {
            timer -= Time.deltaTime;
            UIManager.ShowTimer();

            if (timer <= 0 && !ButtonControl.rpsIsPressed) 
            {
                print("times up");

                timesUp = true;
            }
        }

        if (!isEnding && timesUp)
        {
            if (attack)
            {
                // Player tidak akan melakukan apa2 dan musuh tidak terkena damage
                uiManager.TimerText("TIME'S UP");
            }

            else if (defend)
            {
                // Player akan terkena 1 damage
                uiManager.TimerText("TIME'S UP, YOU TAKE 1 DAMAGE");

                AnimationManager.Animation(AnimationManager.playerAnimator, "isTakingDamage", AnimationManager.enemyAnimator, "isAttacking");

                playerHealth -= 1;
                UIManager.ShowHealth();
            }

            NewState();
        }

        if (!isEnding)
        {
            if (playerHealth <= 0 || enemyHealth <= 0)
            {
                EndCondition();
            }
        }

        if (isEnding)
        {
            UIManager.DisableAllButton();

            restartTimer -= Time.deltaTime;

            if (restartTimer < 0)
            {
                UIManager.RestartPanelControl(true);
            }
        }

        if (timer <= 0) timer = 0;
    }

    void EndCondition()
    {
        isEnding = true;

        StartCoroutine(Condition());
    }

    public void NewState()
    {
        ButtonControl.rpsIsPressed = false;

        if (!isEnding)
        {
            ResetState();

            float rndmNbr = Random.Range(0, 10);

            if (rndmNbr % 2 == 0)
            {
                attack = true;
                defend = false;

                uiManager.CommentText("PLAYER IS ATTACKING", true);
                print("Player Attacking");
            }

            else
            {
                attack = false;
                defend = true;

                uiManager.CommentText("PLAYER IS DEFENDING", true);
                print("Player Defending");
            }

            timer = 15f;
        }

        RPSUIControl.DisableAllRPSIcon();

        ShuffleBtnCondition();

        //UIManager.ShuffleSkill();
        timesUp = false;

        UIManager.infoSkill.SetActive(false);

        EnemyManager.enemyHintPanel.SetActive(false);
        EnemyManager.EnemyRPS();
    }

    static void ShuffleBtnCondition()
    {
        if (SkillManager.cooldownSkill > 0)                                             // Kurangi cooldown count setiap new state
        {
            SkillManager.cooldownSkill -= 1;
            UIManager.cooldownSkilTxt.text = SkillManager.cooldownSkill.ToString();
            UIManager.DisableShuffleBtn(true);
            UIManager.ShuffleBtnIsPressed(true);
        }

        if (SkillManager.cooldownSkill <= 0)
        {
            UIManager.DisableShuffleBtn(false);
            UIManager.ShuffleBtnIsPressed(false);
        }
    }

    static void ResetState()
    {
        attack = false;
        defend = false;
    }

    IEnumerator Condition()
    {
        yield return new WaitForSeconds(2.5f);

        if (playerHealth > 0 && enemyHealth <= 0)
        {
            // WIN GAME
            AnimationManager.Animation(AnimationManager.playerAnimator, "isWinning", AnimationManager.enemyAnimator, "isLosing");

            uiManager.CommentText("YOU WIN THE FIGHT", true);

            yield return null;
        }

        else if (playerHealth <= 0 && enemyHealth > 0)
        {
            // LOSE GAME
            AnimationManager.Animation(AnimationManager.playerAnimator, "isLosing", AnimationManager.enemyAnimator, "isWinning");

            uiManager.CommentText("YOU LOSE THE FIGHT", true);
            
            yield return null;
        }
    }
}
