﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RPSManager : PlayersControl
{
    public static int attackDmg;
    public static int ctrDmg;

    public bool playerWin;
    public bool playerLose;
    public bool even;    

    GameManager gameManager;
    UIManager uiManager;

    private void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        uiManager = GameObject.Find("UIManager").GetComponent<UIManager>();

        attackDmg = 4;
        ctrDmg = 2;
    }

    public void Action()                                 // Digunakan oleh ButtonControl
    {
        Result();

        if (playerWin)
        {
            if (GameManager.attack)
            {
                // animasi player berhasil menyerang
                uiManager.CommentText("YOUR ATTACK SUCCEED, ENEMY TAKES " + attackDmg + " DAMAGE", true);

                StartCoroutine(BattleResultAnimation(AnimationManager.playerAnimator, "isAttacking", AnimationManager.enemyAnimator, "isTakingDamage", "PlayerAttSucceed"));

                GameManager.enemyHealth -= attackDmg;
                UIManager.ShowHealth();
            }

            else
            {
                // animasi player counter musuh
                uiManager.CommentText("YOU COUNTER THE ENEMY, ENEMY TAKES " + ctrDmg + " DAMAGE", true);

                StartCoroutine(BattleResultAnimation(AnimationManager.playerAnimator, "isCountering", AnimationManager.enemyAnimator, "isAttacking", "PlayerAttSucceed"));

                GameManager.enemyHealth -= ctrDmg;
                UIManager.ShowHealth();
            }

            print("PlayerHealth = " + GameManager.playerHealth);
            print("EnemyHealth = " + GameManager.enemyHealth);
        }

        else if (playerLose)
        {
            if (GameManager.attack)
            {
                // animasi musuh counter player
                if (SkillManager.activateShield)
                {
                    uiManager.CommentText("ENEMY COUNTER YOUR ATTACK, YOU TAKES 0 DAMAGE", true);
                    GameManager.playerHealth -= ctrDmg;
                    UIManager.ShowHealth();
                }

                else
                {
                    uiManager.CommentText("ENEMY COUNTER YOUR ATTACK, YOU TAKES 2 DAMAGE", true);
                    GameManager.playerHealth -= 2;
                    UIManager.ShowHealth();
                }

                StartCoroutine(BattleResultAnimation(AnimationManager.playerAnimator, "isAttacking", AnimationManager.enemyAnimator, "isCountering", "EnemyAttSucceed"));                
            }

            else
            {
                // animasi player terkena serangan
                if (SkillManager.activateShield)
                {
                    uiManager.CommentText("ENEMY ATTACK SUCCEED, YOU TAKES 0 DAMAGE", true);
                    GameManager.playerHealth -= ctrDmg;
                    UIManager.ShowHealth();
                }

                else
                {
                    uiManager.CommentText("ENEMY ATTACK SUCCEED, YOU TAKES 4 DAMAGE", true);
                    GameManager.playerHealth -= 4;
                    UIManager.ShowHealth();
                }

                StartCoroutine(BattleResultAnimation(AnimationManager.playerAnimator, "isTakingDamage", AnimationManager.enemyAnimator, "isAttacking", "EnemyAttSucceed"));
            }

            print("PlayerHealth = " + GameManager.playerHealth);
            print("EnemyHealth = " + GameManager.enemyHealth);
        }

        else if (even)
        {
            if (GameManager.attack)
            {
                StartCoroutine(BattleResultAnimation(AnimationManager.playerAnimator, "isAttacking", AnimationManager.enemyAnimator, "isAvoiding", "EnemyAvoid"));

                uiManager.CommentText("IT'S EVEN! THE ENEMY AVOID YOUR ATTACK!", true);
                print("Even");
            }

            else if (GameManager.defend)
            {
                StartCoroutine(BattleResultAnimation(AnimationManager.playerAnimator, "isAvoiding", AnimationManager.enemyAnimator, "isAttacking", "PlayerAvoid"));

                uiManager.CommentText("IT'S EVEN! YOU AVOID THE ENEMY'S ATTACK!", true);
                print("Even");
            }
        }

        SkillManager.ResetSkill();
    }

    void Result()
    {
        ResetStat();

        if (playerRPS == RPS.BATU && enemyRPS == RPS.BATU)
        {
            RPSUIControl.ShowRPSIcon(RPSUIControl.playerRock, RPSUIControl.enemyRock);

            even = true;
        }

        else if (playerRPS == RPS.BATU && enemyRPS == RPS.GUNTING)
        {
            RPSUIControl.ShowRPSIcon(RPSUIControl.playerRock, RPSUIControl.enemyScissor);

            playerWin = true;
        }

        else if (playerRPS == RPS.BATU && enemyRPS == RPS.KERTAS)
        {
            RPSUIControl.ShowRPSIcon(RPSUIControl.playerRock, RPSUIControl.enemyPaper);

            playerLose = true;
        }

        else if (playerRPS == RPS.GUNTING && enemyRPS == RPS.BATU)
        {
            RPSUIControl.ShowRPSIcon(RPSUIControl.playerScissor, RPSUIControl.enemyRock);

            playerLose = true;
        }

        else if (playerRPS == RPS.GUNTING && enemyRPS == RPS.GUNTING)
        {
            RPSUIControl.ShowRPSIcon(RPSUIControl.playerScissor, RPSUIControl.enemyScissor);

            even = true;
        }

        else if (playerRPS == RPS.GUNTING && enemyRPS == RPS.KERTAS)
        {
            RPSUIControl.ShowRPSIcon(RPSUIControl.playerScissor, RPSUIControl.enemyPaper);

            playerWin = true;
        }

        else if (playerRPS == RPS.KERTAS && enemyRPS == RPS.BATU)
        {
            RPSUIControl.ShowRPSIcon(RPSUIControl.playerPaper, RPSUIControl.enemyRock);

            playerWin = true;
        }

        else if (playerRPS == RPS.KERTAS && enemyRPS == RPS.GUNTING)
        {
            RPSUIControl.ShowRPSIcon(RPSUIControl.playerPaper, RPSUIControl.enemyScissor);

            playerLose = true;
        }

        else if (playerRPS == RPS.KERTAS && enemyRPS == RPS.KERTAS)
        {
            RPSUIControl.ShowRPSIcon(RPSUIControl.playerPaper, RPSUIControl.enemyPaper);

            even = true;
        }

        SkillManager.SkillActivated();
    }

    void ResetStat()
    {
        playerWin = false;
        playerLose = false;
        even = false;

        attackDmg = 4;
        ctrDmg = 2;
    }

    IEnumerator BattleResultAnimation(Animator player, string playerCondition, Animator enemy, string enemyCondition, string sound)                        // tambahi dua paramater lagi untuk hasil RPS
    {
        // Tunjukkan hasil RPS apa



        UIManager.DisableAllButton();

        yield return new WaitForSeconds(1.5f);

        AnimationManager.Animation(player, playerCondition, enemy, enemyCondition);

        yield return new WaitForSeconds(0.1f);

        FindObjectOfType<SoundManager>().Play(sound);

        yield return new WaitForSeconds(0.5f);

        if (playerWin)
        {
            AnimationManager.HealthTakingDamageAnimation(AnimationManager.enemyHealthAnimator);
        }

        else if (playerLose)
        {
            AnimationManager.HealthTakingDamageAnimation(AnimationManager.playerHealthAnimator);
        }

        yield return new WaitForSeconds(1.5f);

        gameManager.NewState();                                     // Ganti State

        yield return new WaitForSeconds(1.3f);

        UIManager.EnableAllButton();
        
    }
}
