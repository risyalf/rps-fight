﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RPSUIControl : MonoBehaviour
{
    public static GameObject playerRock;
    public static GameObject playerPaper;
    public static GameObject playerScissor;

    public static GameObject enemyRock;
    public static GameObject enemyPaper;
    public static GameObject enemyScissor;

    private void Start()
    {
        playerRock = GameObject.Find("PlayerRock");
        playerPaper = GameObject.Find("PlayerPaper");
        playerScissor = GameObject.Find("PlayerScissor");

        enemyRock = GameObject.Find("EnemyRock");
        enemyPaper = GameObject.Find("EnemyPaper");
        enemyScissor = GameObject.Find("EnemyScissor");

        DisableAllRPSIcon();
    }

    public static void DisableAllRPSIcon()
    {
        playerRock.SetActive(false);
        playerPaper.SetActive(false);
        playerScissor.SetActive(false);

        enemyRock.SetActive(false);
        enemyPaper.SetActive(false);
        enemyScissor.SetActive(false);
    }

    public static void ShowRPSIcon(GameObject playerSide, GameObject enemySide)
    {
        playerSide.SetActive(true);
        enemySide.SetActive(true);
    }
}
