﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public SoundControl[] sound;

    private void Awake()
    {
        foreach(SoundControl s in sound)
        {
            s.source = gameObject.AddComponent<AudioSource>();

            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
        }
    }

    public void Play(string name)
    {
        SoundControl s = Array.Find(sound, sound => sound.name == name);                        // Mencari nama sesuai name di array sound
        s.source.Play();
    }
}
