﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonControl : MonoBehaviour
{
    public static bool rpsIsPressed;

    RPSManager rpsManager;

    private void Start()
    {
        rpsManager = GameObject.Find("RPSManager").GetComponent<RPSManager>();

        rpsIsPressed = false;
    }

    public void GuntingBtn()
    {
        RPSManager.playerRPS = PlayersControl.RPS.GUNTING;
        rpsManager.Action();

        rpsIsPressed = true;                                       // Digunakan di GameManager pada NewState()
    }

    public void BatuBtn()
    {
        RPSManager.playerRPS = PlayersControl.RPS.BATU;
        rpsManager.Action();

        rpsIsPressed = true;
    }

    public void KertasBtn()
    {
        RPSManager.playerRPS = PlayersControl.RPS.KERTAS;
        rpsManager.Action();

        rpsIsPressed = true;
    }

    public void ActivateSkillShield()                           
    {
        if (!SkillManager.activateShield)
        {
            SkillManager.activateShield = true;
            UIManager.activeText.SetActive(true);
            print("Shield aktif");
        }

        else if (SkillManager.activateShield)
        { 
            SkillManager.activateShield = false;
            UIManager.activeText.SetActive(false);
            print("Shield tidak aktif");
        }

        SkillManager.skillActivate = true;
    }

    public void ActivateSkillCritStrike()
    {
        if (!SkillManager.activateCritStrike)
        {
            SkillManager.activateCritStrike = true;
            UIManager.activeText.SetActive(true);
            print("Crit Strike aktif");
        }

        else if (SkillManager.activateCritStrike)
        {
            SkillManager.activateCritStrike = false;
            UIManager.activeText.SetActive(false);
            print("Crit Strike tidak aktif");
        }

        SkillManager.skillActivate = true;
    }

    public void ActivateSkillHeal()
    {
        if (!SkillManager.activateHeal)
        {
            SkillManager.activateHeal = true; ;
            UIManager.activeText.SetActive(true);
            print("Heal aktif");
        }

        else if (SkillManager.activateHeal)
        {
            SkillManager.activateHeal = false;
            UIManager.activeText.SetActive(false);
            print("Heal tidak aktif");
        }

        SkillManager.skillActivate = true;
    }
}
