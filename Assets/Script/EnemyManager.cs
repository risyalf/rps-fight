﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyManager : MonoBehaviour
{
    public static GameObject enemyHintPanel;
    public static Text enemyHintTxt;

    private void Awake()
    {
        enemyHintPanel = GameObject.Find("EnemyHintPanel");
        enemyHintTxt = GameObject.Find("EnemyHintTxt").GetComponent<Text>();
    }

    private void Start()
    {
        enemyHintPanel.SetActive(false);
    }

    public static void EnemyRPS()                                                // Digunakan di GameManager akhir NewState(), tadinya digunakan di RPSManager di Action()
    {
        float rndmNbr = Random.Range(0, 3);        

        if (rndmNbr == 0)
        {
            PlayersControl.enemyRPS = PlayersControl.RPS.BATU;
            GiveHint("This is HARD...", "I like HARD things", "I'm Hard! As hard as a ROCK!");
        }

        else if (rndmNbr == 1)
        {
            PlayersControl.enemyRPS = PlayersControl.RPS.GUNTING;
            GiveHint("I wanna CUT something.", "I like to CUT!", "Can SCISSOR win over rock?");
        }

        else if (rndmNbr == 2)
        {
            PlayersControl.enemyRPS = PlayersControl.RPS.KERTAS;
            GiveHint("Those magazine have nice MATERIAL....", "Something that can be CUTTED..?", "PAPER is stronger than rock!");
        }
    }

    // Buat fungsi untuk memberika hint

    public static void GiveHint(string txt1, string txt2, string txt3)
    {

        float giveChance = Random.value;

        if(giveChance <= 0.3)
        {
            // target text 
            enemyHintPanel.SetActive(true);

            float whichHint = Random.value;

            if (whichHint < 0.33)
            {
                enemyHintTxt.text = txt1;
            }

            else if(whichHint >= 0.33 && whichHint < 0.66)
            {
                enemyHintTxt.text = txt2;
            }

            else
            {
                enemyHintTxt.text = txt3;
            }

        }
    }

    /*public static void DisableHint()
    {
        enemyHintPanel.SetActive(false);
    }*/
}
